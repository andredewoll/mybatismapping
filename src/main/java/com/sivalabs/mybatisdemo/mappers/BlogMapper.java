package com.sivalabs.mybatisdemo.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
 
import com.sivalabs.mybatisdemo.domain.Blog;

public interface BlogMapper {
	
	public Blog getBlogById(Integer blogId);
	
	public List<Blog> getAllBlogs();
	
}
