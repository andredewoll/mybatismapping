package com.sivalabs.mybatisdemo.mappers;

import java.util.List;
import com.sivalabs.mybatisdemo.domain.User;

public interface UserMapper {

	public User getUserById(Integer userId);

	public List<User> getAllUsers();

}
