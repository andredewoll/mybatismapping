package com.sivalabs.mybatisdemo.service;


import java.util.List;
import org.apache.ibatis.session.SqlSession;
import com.sivalabs.mybatisdemo.domain.User;
import com.sivalabs.mybatisdemo.mappers.UserMapper;

public class UserService implements UserMapper {


	@Override
	public User getUserById(Integer userId) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			return userMapper.getUserById(userId);
		} catch (Exception e) {
			e.printStackTrace();
			session.rollback();
			return null;
		} finally {
			session.close();
		}
	}

	@Override
	public List<User> getAllUsers() {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			return userMapper.getAllUsers();
		} catch (Exception e) {
			e.printStackTrace();
			session.rollback();
			return null;
		} finally {
			session.close();
		}
	}

	

}
