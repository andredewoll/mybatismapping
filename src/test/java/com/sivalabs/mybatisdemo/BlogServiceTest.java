package com.sivalabs.mybatisdemo;


import org.junit.FixMethodOrder;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.sivalabs.mybatisdemo.domain.Blog;
import com.sivalabs.mybatisdemo.service.BlogService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BlogServiceTest {

	private static BlogService blogService;

	@BeforeClass
	public static void setUp() {
		blogService = new BlogService();
	}

	@AfterClass
	public static void tearDown() {
		blogService = null;
	}

	@Test
	public void testBGetBlogById() {
		Blog blog = blogService.getBlogById(1);

		Assert.assertNotNull(blog);
		System.out.println("testing getBlogById: " + blog);
	}


	@Test
	public void testCGetAllBlogs() {
		List<Blog> blogs = blogService.getAllBlogs();

		Assert.assertNotNull(blogs);
		System.out.println("testing getAllBlogs: " + blogs);
	}

}
